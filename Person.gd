extends CharacterBody2D

# Movement variables
var speed = 200

@export var choices = 5
var rng = RandomNumberGenerator.new()

var saturation = 0.5

var base_type = 0
var hair_type = 0
var torso_type = 0
var pants_type = 0
var group = false

var group_node

func set_random_type():
	base_type = rng.randi_range(1, choices)
	hair_type = rng.randi_range(1, choices) 
	torso_type = rng.randi_range(1, choices)
	pants_type = rng.randi_range(1, choices)
	load_sprites()
	set_random_colors()

func set_random_colors():
	var consistent = rng.randf() < 0.5
	if consistent:
		var color = Color.from_hsv(rng.randf(), saturation, 1)
		var hair = get_node("Hair")
		hair.modulate = color
		var torso = get_node("Torso")
		torso.modulate = color
		var pants = get_node("Pants")
		pants.modulate = color
	else:
		var hair = get_node("Hair")
		hair.modulate = Color.from_hsv(rng.randf(), saturation, 1)
		var torso = get_node("Torso")
		torso.modulate = Color.from_hsv(rng.randf(), saturation, 1)
		var pants = get_node("Pants")
		pants.modulate = Color.from_hsv(rng.randf(), saturation, 1)

func load_sprites():
	# Set hair
	var base = get_node("Base")
	base.texture = load("res://assets/sprites/parts/base" + str(base_type) + "_front_idle.png")

	var hair = get_node("Hair")
	hair.texture =  load("res://assets/sprites/parts/hair" + str(hair_type) + "_front_idle.png")

	# Set torso
	var torso = get_node("Torso")
	torso.texture = load("res://assets/sprites/parts/torso" + str(torso_type) + "_front_idle.png")

	# Set pants
	var pants = get_node("Pants")
	pants.texture = load("res://assets/sprites/parts/pants" + str(pants_type) + "_front_idle.png")


func set_group_node(node):
	group_node = node
	$InteractionArea.group_node = node

func _ready():
	set_random_type()

func _physics_process(delta):
	z_index = int(position.y)
	move_and_slide()



func _on_area_2d_body_entered(body):
	pass # Replace with function body.
