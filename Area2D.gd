extends Area2D

var Person = preload("res://Person.tscn")
#var PersonGroup = preload("res://PersonGroup.tscn")

var group_node

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_body_entered(body):
	if body.is_in_group("Person"):
		if not body.group:
			print("Collision")
			if group_node:
				group_node.addPerson(body)
			
			

