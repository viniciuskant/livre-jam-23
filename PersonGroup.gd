extends Node2D

var Person = preload("res://Person.tscn")


# Movement variables
@export var speed = 300
@export var radius = 100
@export var group_size = 5

var rng = RandomNumberGenerator.new()
var center
var assigned_positions
var pointer
var swap_timer = 0
var reordering_timer = 0
var people_node

var spin_angle = 0

var busy = false

enum States {
	NORMAL,
	CONDENSED,
	SPINNING,
	DISPERSED,
}

var _state = States.NORMAL

func on_enter_state(state):
	match _state:
		States.NORMAL:
			radius = 100
			speed = 300
			randomize_assignments()
		States.CONDENSED:
			radius = 20
			speed = 100
			randomize_assignments()
		States.SPINNING:
			radius = 50
			speed = 200
		States.DISPERSED:
			radius = 700
			speed = 500
			randomize_assignments()

func on_exit_state(state):
	pass

func set_state(state):
	on_exit_state(state)
	_state = state
	on_enter_state(state)

func addPersonAsChild(person):
	people_node.add_child(person)
	person.position = center
	person.set_group_node(self)
	randomize_assignments()
	busy = false

func addPerson(person):
	busy = true
	person.group = true
	if person.get_parent():
		person.get_parent().remove_child(person)
	#people_node.add_child(person)
	self.call_deferred("addPersonAsChild", person)
	
# Called when the node enters the scene tree for the first time.
func _ready():
	center = Vector2(get_viewport_rect().size.x / 2, get_viewport_rect().size.y / 2)
	pointer = $Pointer
	people_node = $People
	pointer.position = center
	for i in range(group_size):
		addPerson(Person.instantiate())
	randomize_assignments()

var is_held = {
	"condense": false,
	"spin": false,
	"disperse": false,
	}

func _input(event):
	if event.is_action_pressed("condense"):
		if not is_held["condense"]:
			match _state:
				States.CONDENSED:
					set_state(States.NORMAL)
				_:
					set_state(States.CONDENSED)
		is_held["condense"] = true
	elif event.is_action_released("condense"):
		is_held["condense"] = false
	
	if event.is_action_pressed("spin"):
		if not is_held["spin"]:
			match _state:
				States.SPINNING:
					set_state(States.NORMAL)
				_:
					set_state(States.SPINNING)
		is_held["spin"] = true
	elif event.is_action_released("spin"):
		is_held["spin"] = false
	
	if event.is_action_pressed("disperse"):
		if not is_held["disperse"]:
			match _state:
				States.DISPERSED:
					set_state(States.NORMAL)
				_:
					set_state(States.DISPERSED)
		is_held["disperse"] = true
	elif event.is_action_released("disperse"):
		is_held["disperse"] = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	swap_timer += delta
	reordering_timer += delta
	if swap_timer > 1:
		swap_timer = 0
		noisy_swap()
	
	if reordering_timer > 10:
		reordering_timer = 0
		randomize_assignments()

	var input_vector = Vector2(
		Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
		Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	)

	# Normalize the vector to ensure consistent speed in all directions
	if input_vector.length() > 0:
		input_vector = input_vector.normalized()

		# Calculate velocity
		var velocity = input_vector * speed

		center += velocity * delta
		pointer.position = center
		
	var people = $People.get_children()
	var index = 0

	if not busy:
		for person in people:
			# go to assigned position in relation to the center if it's not already there
			if _state == States.SPINNING:
				var angle = index * 2 * PI / people.size()
				assigned_positions[index] = Vector2(cos(angle + spin_angle), sin(angle + spin_angle)) * radius
				spin_angle = fmod(spin_angle + 1e-3, 2 * PI)

			if (person.position) != (center + assigned_positions[index]):
				person.position.x = move_toward(person.position.x, center.x + assigned_positions[index].x, speed * delta)
				person.position.y = move_toward(person.position.y, center.y + assigned_positions[index].y, speed * delta)

			index += 1

func randomize_assignments():
	# assign positions to people somewhere around the center
	assigned_positions = []

	var people = $People.get_children()
	for person in people:
		var random_radius = rng.randf() * radius  # Random float between 0 and radius
		var random_angle = rng.randf_range(0, 2 * PI)  # Random angle in radians
		var assigned_position = Vector2(cos(random_angle), sin(random_angle)) * random_radius

		assigned_positions.append(assigned_position)

func noisy_swap():
	# swap two random people with some noise
	var people = $People.get_children()
	var index1 = rng.randi() % people.size()
	var index2 = rng.randi() % people.size()
	var index1_pos = assigned_positions[index1]
	assigned_positions[index1] = assigned_positions[index2] + Vector2(rng.randf_range(-0.1, 0.1),
																																	 rng.randf_range(-0.1, 0.1)).normalized() * radius
	if assigned_positions[index1].length() > radius:
		assigned_positions[index1] = assigned_positions[index1].normalized() * radius
	
	assigned_positions[index2] = index1_pos + Vector2(rng.randf_range(-0.1, 0.1),
																																	 rng.randf_range(-0.1, 0.1)).normalized() * radius
	if assigned_positions[index2].length() > radius:
		assigned_positions[index2] = assigned_positions[index2].normalized() * radius


func _on_person_added(person):
	print("person added")
	pass # Replace with function body.
