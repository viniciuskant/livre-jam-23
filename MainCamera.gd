extends Camera2D

@export var target: NodePath
@export var lead_amount: float = 300.0  # How much the camera will lead in front of the target.
@export var lerp_speed: float = 1.5  # How fast the camera moves to the lead or center position.

var last_position: Vector2 = Vector2()
var velocity: Vector2 = Vector2()

func _ready():
	if target:
		last_position = get_node(target).get_node("Pointer").global_position
		print(last_position)

func _physics_process(delta):
	if not target:
		return
	
	var target_node = get_node(target).get_node("Pointer")
	
	# Calculate the target's velocity.
	velocity = (target_node.global_position - last_position) / delta
	
	# Calculate the desired camera position. 
	# It's either directly over the target, or leading in front of it if it's moving.
	var desired_position = target_node.global_position
	if velocity.length() > 0:
		desired_position += velocity.normalized() * lead_amount
	
	# Lerp the camera's position to the desired position.
	global_position = global_position.lerp(desired_position, lerp_speed * delta)
	
	# Update the last position for the next frame.
	last_position = target_node.global_position

